# README #

This is sample code to show how to implement and run [Selenium](http://www.seleniumhq.org/) WebDriver with [Dropbox](http://dropbox.com) GUI. This project is using `Page Object pattern` for Web Elements handling also uses [Dropbox SDK v2.x](https://github.com/dropbox/dropbox-sdk-java) to upload files.

### Design Overview

![code tree](/extra/optile-code.png?raw=true "code tree")


*  You can see list of dependencies [here](/pom.xml)
* Uses `Page Object pattern` to access GUI element. [Pages](/src/main/java/org/optile/qe/pages). [DropboxUI](/src/main/java/org/optile/qe/pages/DropboxUI.java) is the root page object. We can do all the actions from this object.
* Uses [model classes](/src/main/java/org/optile/qe/model) to talk internally
* [projectlombok](https://projectlombok.org/) used for boilerplate codes. You can notice annotations on the code.
* `maven-surefire-plugin` used to run tests from maven
* slf4j logger used with logback. You can change loglevel on [logback.xml](/src/main/resources/logback.xml)
* Curently uses remote web driver on `http://localhost:4444/wd/hub`. however, we can change this on [DropboxUI](/src/main/java/org/optile/qe/pages/DropboxUI.java)

#### Test cases
* Test cases are coded on [QuickTests](/src/test/java/org/optile/qe/tests/QuickTests.java). Have a look here for detailed documentation for tests.


### How do I get set up? ###
* This is [Maven](https://maven.apache.org/) based Java project. It uses [TestNG](http://testng.org/doc/) testing framework.

#### Prerequisites
* Install Maven into your laptop/workstation.
* Install git
* Setup Standalone Selenium Remote WebDriver.
* (Optional) I used to go with [docker-selenium](https://github.com/SeleniumHQ/docker-selenium). If you plan to go with  [docker](https://www.docker.com/) run this command as `sudo` user or `root` user.
    * `docker run -d -p 4444:4444 -p 5901:5900 selenium/standalone-chrome-debug:3.4.0-dysprosium`
    * You can access remote driver via `http://localhost:4444/wd/hub`
    * To watch the UI screen, do VNC to `localhost:5901`(password: `secret`)
* Update [Dropbox](https://www.dropbox.com) UI `username`, `password` and [Dropbox SDK](https://github.com/dropbox/dropbox-sdk-java) [`apiAccessToken`](https://blogs.dropbox.com/developers/2014/05/generate-an-access-token-for-your-own-account/) with real values on the file [testng.xml](src/test/resources/tests/testng.xml)
```xml
  <parameter name="guiUsername" value="user12345"></parameter>
  <parameter name="guiPassword" value="password12345"></parameter>
  <parameter name="apiAccessToken" value="myAccessToken"></parameter>
```

Now, all set ready. We can build and run tests.

#### Execute test

```bash
$ git clone https://bitbucket.org/jkandasa/optile-qe
$ cd optile-qe
$ mvn test
$ ls target/surefire-reports
```
##### Result:
```log
[jkandasa@jkandasa optile-automation]$ mvn test
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building automation 1.0.0
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ automation ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 1 resource
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ automation ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 9 source files to /storage/work/projects/optile-automation/target/classes
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ automation ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ automation ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 2 source files to /storage/work/projects/optile-automation/target/test-classes
[INFO] 
[INFO] --- maven-surefire-plugin:2.20:test (default-test) @ automation ---
[INFO] 
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
[INFO] Running TestSuite
Jul 02, 2017 11:47:49 AM org.openqa.selenium.remote.ProtocolHandshake createSession
INFO: Detected dialect: OSS
2017-07-02 11:48:05,171 DEBUG [main] [org.optile.qe.pages.LoginPage:85] Login success[username:jeeva123@gmail.com]
2017-07-02 11:48:07,503 DEBUG [main] [org.optile.qe.pages.DropboxUI:51] Not logged in. doing login with the user:jeeva123@gmail.com
2017-07-02 11:48:14,331 DEBUG [main] [org.optile.qe.pages.LoginPage:85] Login success[username:jeeva123@gmail.com]
2017-07-02 11:48:15,687 DEBUG [main] [org.optile.qe.pages.FilesPageApi:39] {".tag":"file","name":"upload-test-file.txt","id":"id:zLFQOJR3XTAAAAAAAAAAtw","client_modified":"2017-07-02T06:18:15Z","server_modified":"2017-07-02T06:18:15Z","rev":"19354b35e46","size":40,"path_lower":"/upload-test-file.txt","path_display":"/upload-test-file.txt","content_hash":"42f8de2e528ce4b92a3c5c2a8cc357752b3315c985b4128033e56178501d272c"}
2017-07-02 11:48:18,915 DEBUG [main] [org.optile.qe.pages.FilesPage:261] Number of file(s):2
2017-07-02 11:48:21,428 DEBUG [main] [org.optile.qe.pages.FilesPage:283] Files:[DropboxFile(name=Get Started with Dropbox.pdf, modified=29/6/2017 5:32 PM, members=Only you, type=Document, extension=pdf, size=884.60 KB), DropboxFile(name=upload-test-file.txt, modified=1 sec ago, members=Only you, type=Document, extension=txt, size=40 bytes)]
2017-07-02 11:48:27,872 DEBUG [main] [org.optile.qe.pages.FilesPage:98] Folder creation Status(success=true, message=Created folder folder123.)
2017-07-02 11:48:31,576 DEBUG [main] [org.optile.qe.pages.FilesPage:261] Number of file(s):3
2017-07-02 11:48:35,361 DEBUG [main] [org.optile.qe.pages.FilesPage:283] Files:[DropboxFile(name=folder123, modified=--, members=Only you, type=Folder, extension=--, size=--), DropboxFile(name=Get Started with Dropbox.pdf, modified=29/6/2017 5:32 PM, members=Only you, type=Document, extension=pdf, size=884.60 KB), DropboxFile(name=upload-test-file.txt, modified=6 secs ago, members=Only you, type=Document, extension=txt, size=40 bytes)]
2017-07-02 11:48:36,174 DEBUG [main] [org.optile.qe.pages.FilesPageApi:75] [DropboxFile(name=folder123, modified=null, members=Only you, type=Folder, extension=null, size=null), DropboxFile(name=Get Started with Dropbox.pdf, modified=Thu Jun 29 17:32:03 IST 2017, members=Only you, type=null, extension=pdf, size=905827), DropboxFile(name=upload-test-file.txt, modified=Sun Jul 02 11:48:15 IST 2017, members=Only you, type=null, extension=txt, size=40)]
2017-07-02 11:48:48,746 DEBUG [main] [org.optile.qe.pages.FilesPage:247] Resource rename Status(success=true, message=Rename complete. Undo), name:folder123, new-name:my-name-was_folder123
2017-07-02 11:48:52,260 DEBUG [main] [org.optile.qe.pages.FilesPage:261] Number of file(s):3
2017-07-02 11:48:56,090 DEBUG [main] [org.optile.qe.pages.FilesPage:283] Files:[DropboxFile(name=my-name-was_folder123, modified=--, members=Only you, type=Folder, extension=--, size=--), DropboxFile(name=Get Started with Dropbox.pdf, modified=29/6/2017 5:32 PM, members=Only you, type=Document, extension=pdf, size=884.60 KB), DropboxFile(name=upload-test-file.txt, modified=24 secs ago, members=Only you, type=Document, extension=txt, size=40 bytes)]
2017-07-02 11:48:56,693 DEBUG [main] [org.optile.qe.pages.FilesPageApi:75] [DropboxFile(name=my-name-was_folder123, modified=null, members=Only you, type=Folder, extension=null, size=null), DropboxFile(name=Get Started with Dropbox.pdf, modified=Thu Jun 29 17:32:03 IST 2017, members=Only you, type=null, extension=pdf, size=905827), DropboxFile(name=upload-test-file.txt, modified=Sun Jul 02 11:48:15 IST 2017, members=Only you, type=null, extension=txt, size=40)]
2017-07-02 11:49:09,555 DEBUG [main] [org.optile.qe.pages.FilesPage:199] Resource creation Status(success=true, message=Deleted 1 item. Undo)
2017-07-02 11:49:12,991 DEBUG [main] [org.optile.qe.pages.FilesPage:261] Number of file(s):2
2017-07-02 11:49:15,498 DEBUG [main] [org.optile.qe.pages.FilesPage:283] Files:[DropboxFile(name=Get Started with Dropbox.pdf, modified=29/6/2017 5:32 PM, members=Only you, type=Document, extension=pdf, size=884.60 KB), DropboxFile(name=upload-test-file.txt, modified=45 secs ago, members=Only you, type=Document, extension=txt, size=40 bytes)]
2017-07-02 11:49:16,298 DEBUG [main] [org.optile.qe.pages.FilesPageApi:75] [DropboxFile(name=Get Started with Dropbox.pdf, modified=Thu Jun 29 17:32:03 IST 2017, members=Only you, type=null, extension=pdf, size=905827), DropboxFile(name=upload-test-file.txt, modified=Sun Jul 02 11:48:15 IST 2017, members=Only you, type=null, extension=txt, size=40)]
2017-07-02 11:49:30,260 DEBUG [main] [org.optile.qe.pages.FilesPage:199] Resource creation Status(success=true, message=Permanently deleted 1 item.)
2017-07-02 11:49:33,731 DEBUG [main] [org.optile.qe.pages.FilesPage:261] Number of file(s):0
2017-07-02 11:49:33,732 DEBUG [main] [org.optile.qe.pages.FilesPage:283] Files:[]
2017-07-02 11:49:34,323 DEBUG [main] [org.optile.qe.pages.FilesPageApi:75] [DropboxFile(name=Get Started with Dropbox.pdf, modified=Thu Jun 29 17:32:03 IST 2017, members=Only you, type=null, extension=pdf, size=905827), DropboxFile(name=upload-test-file.txt, modified=Sun Jul 02 11:48:15 IST 2017, members=Only you, type=null, extension=txt, size=40)]
2017-07-02 11:49:46,756 DEBUG [main] [org.optile.qe.pages.FilesPage:199] Resource creation Status(success=true, message=Deleted 1 item. Undo)
2017-07-02 11:50:01,849 DEBUG [main] [org.optile.qe.pages.FilesPage:199] Resource creation Status(success=true, message=Permanently deleted 1 item.)
2017-07-02 11:50:05,181 DEBUG [main] [org.optile.qe.pages.FilesPage:261] Number of file(s):0
2017-07-02 11:50:05,182 DEBUG [main] [org.optile.qe.pages.FilesPage:283] Files:[]
[INFO] Tests run: 6, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 137.455 s - in TestSuite
[INFO] 
[INFO] Results:
[INFO] 
[INFO] Tests run: 6, Failures: 0, Errors: 0, Skipped: 0
[INFO] 
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 02:23 min
[INFO] Finished at: 2017-07-02T11:50:06+05:30
[INFO] Final Memory: 23M/369M
[INFO] ------------------------------------------------------------------------

```
Also, you can see the results in the directory >> `target/surefire-reports`. For `HTML` report `target/surefire-reports/index.html`

You can see the [sample results](extra/surefire-reports)