package org.optile.qe.tests;

import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.optile.qe.model.DropboxFile;
import org.optile.qe.model.Status;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class QuickTests extends BaseTest {

    /*
     * On exit clears existing files created by this tests
     * Steps:
     * 1. delete "uploaded" file
     * 2. delete Permanently
     */
    @AfterClass
    @Parameters({ "uploadFile" })
    public void afterClass(String fileName) {
        List<DropboxFile> files = ui().files().api().listFiles();
        DropboxFile file = getDbFile(files, FilenameUtils.getName(fileName));
        //Check uploaded file is available on UI
        if (file != null) {
            //do delete
            Status status = ui().files().deleteFile(file.getName());
            Assert.assertTrue(status.isSuccess(), status.toString());
            //do permanent delete
            ui().files().deleteFilePermanently(file.getName());
            Assert.assertNull(getDbFile(ui().files().listFiles(true/*isShowDeletedFiles*/), file.getName()));
        }
    }

    /*Test Scenario #1. Login/Logout functionality
     * Steps:
     * 1. do login with valid user
     * 2. validate login status
     * 3. do logout
     * 4. validate logout status
     */
    @Test(priority = 0)
    @Parameters({ "guiUsername", "guiPassword" })
    public void loginLogoutTest(String username, String password) {
        //do login and get status
        Status status = ui().login().doLogin(username, password, false/*rememberMe*/);

        //validate login status
        Assert.assertTrue(status.isSuccess(), status.toString());

        //do logout
        ui().login().doLogout();

        //validate logout status
        Assert.assertFalse(ui().login().isLoggedin());
    }

    /*Test Scenario #2. Upload file functionality
     * Steps:
     * 1. upload file via Dropbox SDK api
     * 2. list files via GUI
     * 3. validate file is available on gui
     */
    @Test(priority = 1)
    @Parameters({ "uploadFile" })
    public void fileUploadTest(String uploadFile) {
        //Upload a file via dropbox sdk api
        Status status = ui().files().api().uploadFile(uploadFile);
        Assert.assertTrue(status.isSuccess(), status.toString());

        //validate upload status
        Assert.assertNotNull(getDbFile(ui().files().listFiles(), FilenameUtils.getName(uploadFile)));
    }

    /*Test Scenario #3. Create folder functionality
     * Steps:
     * 1. create new folder
     * 2. get list from UI
     * 3. validate from the UI list
     * 4. get list from API
     * 5. validate from api list
     */
    @Test(priority = 2)
    @Parameters({ "createFolder" })
    public void createFolderTest(String folderName) {
        //Create folder
        Status status = ui().files().createFolder(folderName);
        Assert.assertTrue(status.isSuccess(), status.toString());

        //validate creation status on GUI
        Assert.assertNotNull(getDbFile(ui().files().listFiles(), folderName));

        //validate creation status on API
        Assert.assertNotNull(getDbFile(ui().files().api().listFiles(), folderName));
    }

    /*Test Scenario #4. rename folder functionality
     * Steps:
     * 1. select existing folder and do rename (append string "my-name-was_" with original name)
     * 2. validate on UI
     * 3. validate on API
     */
    @Test(dependsOnMethods = "createFolderTest", priority = 3)
    @Parameters({ "createFolder" })
    public void renameFolderTest(String folderName) {
        String newFolderName = "my-name-was_" + folderName;
        //rename folder
        Status status = ui().files().renameFolder(folderName, newFolderName);
        Assert.assertTrue(status.isSuccess(), status.toString());

        //validate rename status on GUI
        Assert.assertNotNull(getDbFile(ui().files().listFiles(), newFolderName));

        //validate rename status on API
        Assert.assertNotNull(getDbFile(ui().files().api().listFiles(), newFolderName));
    }

    /*Test Scenario #5. delete folder functionality
     * Steps:
     * 1. delete a folder (selects and deletes input folder)
     * 2. validate on UI
     * 3. validate on API
     */
    @Test(dependsOnMethods = "renameFolderTest", priority = 4)
    @Parameters({ "createFolder" })
    public void deleteFolderTest(String folderName) {
        folderName = "my-name-was_" + folderName;
        //delete folder
        Status status = ui().files().deleteFolder(folderName);
        Assert.assertTrue(status.isSuccess(), status.toString());

        //validate deletion status on GUI
        Assert.assertNull(getDbFile(ui().files().listFiles(), folderName));

        //validate deletion status on API
        Assert.assertNull(getDbFile(ui().files().api().listFiles(), folderName));
    }

    /*Test Scenario #6. Permanently delete folder functionality
     * Steps:
     * 1. select a folder from hidden list and delete permanently
     * 2. validate on UI
     */
    @Test(dependsOnMethods = "deleteFolderTest", priority = 5)
    @Parameters({ "createFolder" })
    public void permanentlyDeleteFolderTest(String folderName) {
        folderName = "my-name-was_" + folderName;
        //Create folder
        Status status = ui().files().deleteFolderPermanently(folderName);
        Assert.assertTrue(status.isSuccess(), status.toString());

        //validate creation status on GUI
        Assert.assertNull(getDbFile(ui().files().listFiles(true/*isShowDeletedFiles*/), folderName));
    }

}
