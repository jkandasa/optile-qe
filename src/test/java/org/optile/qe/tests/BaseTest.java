package org.optile.qe.tests;

import java.util.List;

import org.optile.qe.model.DropboxFile;
import org.optile.qe.pages.DropboxUI;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

public class BaseTest {
    private DropboxUI ui = null;

    /*
     * load dropbox ui page objects
     * */
    @BeforeSuite
    @Parameters({ "guiUsername", "guiPassword", "apiAccessToken" })
    public void beforeSuite(ITestContext context, String guiUsername, String guiPassword, String apiAccessToken) {
        ui = new DropboxUI(guiUsername, guiPassword, apiAccessToken);
    }

    /*
     * close browser on exit
     * */
    @AfterSuite
    public void afterSuite() {
        ui().close();
    }

    public DropboxUI ui() {
        return ui;
    }

    public DropboxFile getDbFile(List<DropboxFile> sourceList, String fileName) {
        for (DropboxFile data : sourceList) {
            if (data.getName().equals(fileName)) {
                return data;
            }
        }
        return null;
    }

    public void assertDbFileList(List<DropboxFile> listActual, List<DropboxFile> listExpected) {
        Assert.assertEquals(listActual.size(), listExpected.size());
        for (DropboxFile expected : listExpected) {
            DropboxFile actual = getDbFile(listActual, expected.getName());
            Assert.assertNotNull(actual);
            Assert.assertEquals(actual.getName(), expected.getName());
        }
    }
}
