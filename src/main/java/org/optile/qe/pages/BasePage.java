package org.optile.qe.pages;

import static java.util.concurrent.TimeUnit.SECONDS;

import static org.awaitility.Awaitility.await;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class BasePage {
    public static final String ROOT_URL = "https://www.dropbox.com";
    private WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    protected WebDriver driver() {
        return driver;
    }

    public boolean isPresent(By by) {
        try {
            return driver().findElement(by) != null;
        } catch (Exception ex) {
            _logger.trace("Exception, ", ex);
            return false;
        }
    }

    public boolean isDisplayed(By by) {
        if (isPresent(by)) {
            try {
                return driver().findElement(by).isDisplayed();
            } catch (Exception ex) {
                _logger.trace("Exception, ", ex);
                return false;
            }
        }
        return false;
    }

    public boolean isEnabled(By by) {
        if (isPresent(by)) {
            try {
                return driver().findElement(by).isEnabled();
            } catch (Exception ex) {
                _logger.trace("Exception, ", ex);
                return false;
            }
        }
        return false;
    }

    public void maximize() {
        this.driver().manage().window().maximize();
    }

    public void close() {
        this.driver().close();
    }

    public void loadUrl(String url) {
        if (url.startsWith("/")) {
            this.driver().get(ROOT_URL + url);
        } else {
            this.driver().get(url);
        }
        maximize();
    }

    public void waitUntillPresentSafe(By element) {
        try {
            waitUntillPresent(element);
        } catch (Exception ex) {
            _logger.trace("exception, ", ex);
        }
    }

    public void waitUntillDisappearSafe(By element) {
        try {
            waitUntillDisappear(element);
        } catch (Exception ex) {
            _logger.trace("exception, ", ex);
        }
    }

    public void waitUntillPresent(By element) {
        await().with().pollInterval(1, SECONDS).atMost(30, SECONDS).until(() -> isPresent(element));
    }

    public void waitUntillDisappear(By element) {
        await().with().pollInterval(1, SECONDS).atMost(30, SECONDS).until(() -> !isPresent(element));
    }

}
