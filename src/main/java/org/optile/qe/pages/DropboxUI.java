package org.optile.qe.pages;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.optile.qe.model.Status;

import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DropboxUI {
    private LoginPage loginPage;
    private FilesPage filesPage;

    private String guiUsername;
    private String guiPassword;
    private String apiAccessToken;

    private DbxClientV2 apiClient;

    private WebDriver webDriver;

    public DropboxUI(String guiUsername, String guiPassword, String apiAccessToken) {
        this.guiUsername = guiUsername;
        this.guiPassword = guiPassword;
        this.apiAccessToken = apiAccessToken;
        apiClient = new DbxClientV2(new DbxRequestConfig("automation-code"), this.apiAccessToken);
        try {
            // if you want to change browser and/or remote driver location, do it here.
            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            webDriver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }
        loginPage = new LoginPage(webDriver);
        filesPage = new FilesPage(webDriver, apiClient);
    }

    public LoginPage login() {
        return loginPage;
    }

    public FilesPage files() {
        if (!login().isLoggedin()) {
            _logger.debug("Not logged in. doing login with the user:{}", guiUsername);
            doDefaultLogin();
        }
        return filesPage;
    }

    public WebDriver driver() {
        return webDriver;
    }

    public DbxClientV2 api() {
        return apiClient;
    }

    public Status doDefaultLogin() {
        return login().doLogin(guiUsername, guiPassword, true/*Remember me*/);
    }

    public void close() {
        driver().close();
    }
}
