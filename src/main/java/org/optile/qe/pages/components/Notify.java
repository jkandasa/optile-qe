package org.optile.qe.pages.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.optile.qe.pages.BasePage;

public class Notify extends BasePage {
    public Notify(WebDriver driver) {
        super(driver);
    }

    public static By _successMsg = By
            .xpath("//*[@id='notify-wrapper']/*[@id='notify' and @class='server-success']/*[@id='notify-msg']");
    public static By _errorMsg = By
            .xpath("//*[@id='notify-wrapper']/*[@id='notify' and @class='server-error']/*[@id='notify-msg']");
    private By _allMsg = By
            .xpath("//*[@id='notify-wrapper']/*[@id='notify']/*[@id='notify-msg']");

    private String getMessage(By element) {
        if (isPresent(element) /*&& isDisplayed(element)*/) {
            return driver().findElement(element).getText();
        }
        return null;
    }

    public String getMessage() {
        return getMessage(_allMsg);
    }

    public String getSuccessMessage() {
        return getMessage(_successMsg);
    }

    public String getErrorMessage() {
        return getMessage(_errorMsg);
    }
}
