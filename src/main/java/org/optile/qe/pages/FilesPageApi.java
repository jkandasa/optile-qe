package org.optile.qe.pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.optile.qe.model.DropboxFile;
import org.optile.qe.model.Status;

import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.FolderMetadata;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.files.WriteMode;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FilesPageApi {
    private DbxClientV2 apiClient;

    public FilesPageApi(DbxClientV2 apiClient) {
        this.apiClient = apiClient;
    }

    public Status uploadFile(String fileNameWithPath) {
        try {
            File file = FileUtils.getFile(fileNameWithPath);
            InputStream in = new FileInputStream(file);
            FileMetadata metadata = apiClient.files().uploadBuilder("/" + file.getName())
                    .withMode(WriteMode.ADD)
                    .uploadAndFinish(in);
            _logger.debug("{}", metadata);
            return Status.builder().success(true).message(metadata.toString()).build();
        } catch (DbxException | IOException ex) {
            _logger.error("Exception, ", ex);
            return Status.builder().success(false).message(ex.getMessage()).build();
        }
    }

    public List<DropboxFile> listFiles() {
        List<DropboxFile> dbFiles = new ArrayList<DropboxFile>();

        try {
            List<Metadata> files = apiClient.files().listFolder("").getEntries();
            for (Metadata file : files) {
                if (file instanceof FileMetadata) {
                    FileMetadata fm = (FileMetadata) file;
                    dbFiles.add(DropboxFile.builder()
                            .name(fm.getName())
                            .size(String.valueOf(fm.getSize()))
                            .modified(String.valueOf(fm.getServerModified()))
                            .members(fm.getSharingInfo() == null ? "Only you" : fm.getSharingInfo().toString())
                            .extension(FilenameUtils.getExtension(fm.getName()))
                            .build());
                } else if (file instanceof FolderMetadata) {
                    FolderMetadata fm = (FolderMetadata) file;
                    dbFiles.add(DropboxFile.builder()
                            .name(fm.getName())
                            .type("Folder")
                            .members(fm.getSharingInfo() == null ? "Only you" : fm.getSharingInfo().toString())
                            .build());
                }

            }
        } catch (DbxException ex) {
            _logger.error("Exception, ", ex);
        }
        _logger.debug("{}", dbFiles);
        return dbFiles;

    }

}
