package org.optile.qe.pages;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.optile.qe.model.Status;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LoginPage extends BasePage {
    private String _logoutUrl = "/logout";
    private By _username = By.xpath("//*[@id='regular-login-forms']//input[@name='login_email']");
    private By _password = By.xpath("//*[@id='regular-login-forms']//input[@name='login_password']");
    private By _rememberMe = By.xpath("//*[@id='regular-login-forms']//input[@name='remember_me']");
    private By _submitButton = By.xpath("//*[@id='regular-login-forms']//button[contains(@class, 'login-button')]");
    private By _loginForm = By.xpath("//*[@id='regular-login-forms']");
    private By _emailError = By.xpath("//*[contains(@class, 'error-message')]");

    private By _afterLogin = By.xpath("//button[contains(@aria-label, 'Account menu')]");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void load() {
        load(false);
    }

    public void load(boolean forceReload) {
        if (forceReload || !isPreset()) {
            doLogout();
        }
    }

    private boolean isLoginDone() {
        if (isPresent(_afterLogin)) {
            return true;
        } else if (isEnabled(_submitButton)) {
            return true;
        }
        return false;
    }

    public boolean isLoggedin() {
        return isPresent(_afterLogin);
    }

    public void doLogout() {
        loadUrl(_logoutUrl);
    }

    public Status doLogin(String username, String password) {
        return doLogin(username, password, false);
    }

    public Status doLogin(String username, String password, boolean rememberMe) {
        this.load();
        // Check, if already logged in. do logout
        doLogout();
        // Enter email address/User name
        driver().findElement(_username).sendKeys(username);
        // Enter password
        driver().findElement(_password).sendKeys(password);
        // Check or uncheck 'Remember me'. checkbox
        if (rememberMe && !driver().findElement(_rememberMe).isSelected()) {
            driver().findElement(_rememberMe).click();
        } else if (!rememberMe
                && driver().findElement(_rememberMe).isSelected()) {
            driver().findElement(_rememberMe).click();
        }
        // Click on submit button
        driver().findElement(_submitButton).click();
        await().with().pollInterval(1, SECONDS).atMost(30, SECONDS).until(() -> isLoginDone());
        // Check still on login page, if so check error message
        if (isPreset()) {
            String msg = isPresent(_emailError) ? driver().findElement(
                    _emailError).getText() : null;
            _logger.debug("Login failed[username:{}, error message:{}]",
                    username, msg);
            return Status.builder().success(false).message(msg).build();
        }
        _logger.debug("Login success[username:{}]", username);
        return Status.builder().success(true).build();
    }

    public boolean isPreset() {
        return isPresent(_loginForm);
    }
}