package org.optile.qe.pages;

import static java.util.concurrent.TimeUnit.SECONDS;

import static org.awaitility.Awaitility.await;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.optile.qe.model.DropboxFile;
import org.optile.qe.model.Status;
import org.optile.qe.pages.components.Notify;

import com.dropbox.core.v2.DbxClientV2;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FilesPage extends BasePage {
    private static final String _filesPageUrl = "/home";

    private Notify notify = null;

    private FilesPageApi api = null;

    public FilesPage(WebDriver driver, DbxClientV2 apiClient) {
        super(driver);
        this.api = new FilesPageApi(apiClient);
        notify = new Notify(driver);
    }

    private static final String FILES = "//div[@class='browse-files-wrapper']//ul[contains(@class, 'browse-files')]";
    //private static final String FILES_LIST = FILES + "/li[contains(@class, 'browse-file-row')]";
    private static final String FILES_LIST = FILES + "/li";

    private static final String MODEL = "//*[@id='db-modal-title']";

    private By _filesList = By.xpath(FILES_LIST);

    //Create folder
    private By _buttonNewFolder = By
            .xpath("//*[@class='appactions-menu']//button[contains(@class, 'action-new-folder')]");
    private By _inputNewFolder = By.xpath(FILES_LIST + "//input[@aria-label='Folder name']");
    private By _textCreatingFolder = By.xpath(FILES_LIST + "//*[@class='creating_folder_text']");

    //Delete resource
    private By _buttonDeleteResource = By
            .xpath("//*[@class='appactions-menu']//button[contains(@class, 'action-delete')]");
    private By _buttonDeleteConfirmation = By.xpath("//*[@class='db-modal-buttons']//button[text()='Delete']");
    private By _buttonShowDeletedResources = By
            .xpath("//*[@class='appactions-menu']//button[contains(@class, 'action-deleted-files')]");
    private By _buttonPermanentlyDeleteResource = By
            .xpath("//*[@class='appactions-menu']//button[contains(@class, 'action-permanently-delete')]");
    private By _buttonPermanentlyDeleteConfirmation = By
            .xpath("//*[@class='db-modal-buttons']//button[text()='Delete permanently']");

    //Rename resource
    private By _buttonRenameResource = By
            .xpath("//*[@class='appactions-menu']//button[contains(@class, 'action-rename')]");
    private By _inputRenameFolder = By.xpath(FILES_LIST + "//input[@aria-label='Rename directory…']");

    //Files loading indicator
    private By _iconLoadingIndicator = By.xpath("//*[contains(@class, 'item-list-loading-indicator-wrapper']");

    public FilesPageApi api() {
        return api;
    }

    public Status createFolder(String folderName) {
        //load files page
        load();

        //Click create new folder button
        driver().findElement(_buttonNewFolder).click();

        //Wait for "New folder" input box
        waitUntillPresent(_inputNewFolder);

        //create new folder
        driver().findElement(_inputNewFolder).sendKeys(folderName);
        driver().findElement(_inputNewFolder).sendKeys(Keys.ENTER);

        //Wait until folder creating
        waitUntillDisappear(_textCreatingFolder);

        //Check error messages
        Status status = null;
        if (notify.getErrorMessage() != null) {
            status = Status.builder().success(false).message(notify.getErrorMessage()).build();
        } else if (notify.getSuccessMessage() != null) {
            status = Status.builder().success(true).message(notify.getSuccessMessage()).build();
        } else {
            status = Status.builder().success(false).message(notify.getMessage()).build();
        }
        _logger.debug("Folder creation {}", status);
        return status;
    }

    private void selectResource(String resourceName, boolean isFolder, boolean isPermanentlyDelete) {
        //load files page
        load();

        //Show deleted resources
        if (isPermanentlyDelete) {
            driver().findElement(_buttonShowDeletedResources).click();
            waitUntillDisappearSafe(_iconLoadingIndicator);
        }

        if (isPermanentlyDelete) {
            resourceName += " Deleted";
        }

        //if it is a folder add 'Folder' along with this resource name
        if (isFolder) {
            resourceName += " Folder";
        }
        By resource = By.xpath(FILES + "/li[div = '" + resourceName
                + "']//button[contains(@class, 'checkbox-button')]");
        //Give some time to display the resource
        waitUntillPresentSafe(resource);
        driver().findElement(resource).click();
    }

    public Status deleteFilePermanently(String fileName) {
        return deleteResource(fileName, false/*isFolder*/, true /*isPermanentlyDelete*/);
    }

    public Status deleteFolderPermanently(String folderName) {
        return deleteResource(folderName, true/*isFolder*/, true /*isPermanentlyDelete*/);
    }

    public Status deleteFile(String fileName) {
        return deleteResource(fileName, false/*isFolder*/, false /*isPermanentlyDelete*/);
    }

    public Status deleteFolder(String folderName) {
        return deleteResource(folderName, true/*isFolder*/, false /*isPermanentlyDelete*/);
    }

    private Status deleteResource(String resourceName, boolean isFolder, boolean isPermanentlyDelete) {
        //load files page
        load();

        //Select resource
        selectResource(resourceName, isFolder, isPermanentlyDelete);

        //Click delete button
        if (isPermanentlyDelete) {
            waitUntillPresent(_buttonPermanentlyDeleteResource);
            driver().findElement(_buttonPermanentlyDeleteResource).click();
        } else {
            waitUntillPresent(_buttonDeleteResource);
            driver().findElement(_buttonDeleteResource).click();
        }

        By model = null;
        if (isPermanentlyDelete) {
            model = By.xpath(MODEL + "//*[text()='Permanently delete 1 file?']");
        } else {
            if (isFolder) {
                model = By.xpath(MODEL + "//*[text()='Delete folder?']");
            } else {
                model = By.xpath(MODEL + "//*[text()='Delete file?']");
            }
        }

        //Wait until the model comes
        waitUntillPresent(model);

        if (isPermanentlyDelete) {
            driver().findElement(_buttonPermanentlyDeleteConfirmation).click();

        } else {
            driver().findElement(_buttonDeleteConfirmation).click();
        }

        //Wait till deletion completes
        waitUntillPresentSafe(Notify._successMsg);

        //Wait till deleted resource disappear
        if (isFolder) {
            waitUntillDisappearSafe(By.xpath(FILES + "/li[div = '" + resourceName + " Folder']"));
        } else {
            waitUntillDisappearSafe(By.xpath(FILES + "/li[div = '" + resourceName + "']"));
        }

        //Check error messages
        Status status = null;
        if (notify.getErrorMessage() != null) {
            status = Status.builder().success(false).message(notify.getErrorMessage()).build();
        } else if (notify.getSuccessMessage() != null) {
            status = Status.builder().success(true).message(notify.getSuccessMessage()).build();
        } else {
            status = Status.builder().success(false).message(notify.getMessage()).build();
        }
        _logger.debug("Resource creation {}", status);
        return status;
    }

    public Status renameFile(String fileName, String newName) {
        return renameResource(fileName, newName, false/*isFolder*/);
    }

    public Status renameFolder(String folderName, String newName) {
        return renameResource(folderName, newName, true/*isFolder*/);
    }

    private Status renameResource(String resourceName, String newName, boolean isFolder) {
        //load files page
        load();

        //Select resource
        selectResource(resourceName, isFolder, false/*isPermanentlyDelete*/);

        //Click rename button
        waitUntillPresent(_buttonRenameResource);
        driver().findElement(_buttonRenameResource).click();

        //Wait for rename input
        waitUntillPresent(_inputRenameFolder);

        driver().findElement(_inputRenameFolder).sendKeys(newName);
        driver().findElement(_inputRenameFolder).sendKeys(Keys.ENTER);

        //Wait till deletion completes
        waitUntillPresentSafe(Notify._successMsg);

        //Wait till renamed value update
        if (isFolder) {
            waitUntillPresentSafe(By.xpath(FILES + "/li[div = '" + newName + " Selected Folder']"));
        } else {
            waitUntillPresentSafe(By.xpath(FILES + "/li[div = '" + newName + "  Selected']"));
        }

        //Check error messages
        Status status = null;
        if (notify.getErrorMessage() != null) {
            status = Status.builder().success(false).message(notify.getErrorMessage()).build();
        } else if (notify.getSuccessMessage() != null) {
            status = Status.builder().success(true).message(notify.getSuccessMessage()).build();
        } else {
            status = Status.builder().success(false).message(notify.getMessage()).build();
        }
        _logger.debug("Resource rename {}, name:{}, new-name:{}", status, resourceName, newName);
        return status;
    }

    public List<DropboxFile> listFiles() {
        return listFiles(false/*isShowDeletedFiles*/);
    }

    public List<DropboxFile> listFiles(boolean isShowDeletedFiles) {
        load();
        if (isShowDeletedFiles) {
            driver().findElement(_buttonShowDeletedResources).click();
        }
        int noOfElements = driver().findElements(_filesList).size();
        _logger.debug("Number of file(s):{}", noOfElements);
        List<DropboxFile> files = new ArrayList<DropboxFile>();
        for (int index = 1; index <= noOfElements; index++) {
            String locator = FILES_LIST + "[" + index + "]";
            waitUntillPresentSafe(By.xpath(locator
                    + "//*[contains(@class, 'column--sharedwith')]//*[@class='audience-description']"));
            files.add(DropboxFile
                    .builder()
                    .name(driver().findElement(By.xpath(
                            locator + "//*[contains(@class, 'column--filename')]//a/span[1]")).getText())
                    .modified(driver().findElement(By.xpath(
                            locator + "//*[contains(@class, 'column--modified')]")).getText())
                    .members(driver().findElement(By.xpath(
                            locator + "//*[contains(@class, 'column--sharedwith')]")).getAttribute("textContent"))
                    .extension(driver().findElement(By.xpath(
                            locator + "//*[contains(@class, 'column--extension')]")).getAttribute("textContent"))
                    .type(driver().findElement(By.xpath(
                            locator + "//*[contains(@class, 'column--type')]")).getAttribute("textContent"))
                    .size(driver().findElement(By.xpath(
                            locator + "//*[contains(@class, 'column--size')]")).getAttribute("textContent"))
                    .build());
        }
        _logger.debug("Files:{}", files);
        return files;
    }

    private void load() {
        loadUrl(_filesPageUrl);
        await().with().pollInterval(1, SECONDS).atMost(30, SECONDS)
                .until(() -> driver().getCurrentUrl().endsWith(_filesPageUrl));
        //Wait for the icon loading
        waitUntillDisappearSafe(_iconLoadingIndicator);
    }

}
