package org.optile.qe.dbsdk;

import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;

public class DropboxApi extends DbxClientV2 {

    public DropboxApi(DbxRequestConfig requestConfig, String accessToken) {
        super(requestConfig, accessToken);
    }

    public DropboxApi(String accessToken) {
        super(new DbxRequestConfig("automation-work"), accessToken);
    }

}
