package org.optile.qe.model;

import lombok.ToString;

import lombok.Getter;
import lombok.Builder;

@Builder
@Getter
@ToString
public class Status {
    private boolean success;
    private String message;
}
