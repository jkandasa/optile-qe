package org.optile.qe.model;

import lombok.Builder;

import lombok.Getter;
import lombok.ToString;

@Builder
@ToString
@Getter
public class DropboxFile {
    private String name;
    private String modified;
    private String members;
    private String type;
    private String extension;
    private String size;
}
